#!/bin/bash

# Script to pack all textures and files to one blender file
# The script search after blender files in directory pack and transfer them to other directory 
# author: meirg
# MIT - 2020

usage="Script to pack all textures and files to one blender project.

Usage: $(basename "$0") [-sd [-loh] ]
    -h   show this help text
    -s   set source location to search *.blend files
    -d   set destination location to build the project
    -l   set search level depth value (default: 1)
    -o   move all resources like textures to the project directory"

# Defaults:
level=1

while getopts d:s:l:oh option
    do case "${option}" in
        h) echo "$usage"; exit 1;;
        d) dest=${OPTARG};;
        s) src=${OPTARG};;
        l) level=${OPTARG};;
        o) organize=true;;
       \?) echo "$usage" >&2 ; exit 1;;
    esac
done

# Exit if argument not set
if [ -z "$dest" ] || [ -z "$src" ]; then
    echo 'Error: one or more variables are undefined'
  exit 1
else
    dest="$dest"
fi

function Organize_files() {
    if [ ! -z "$organize" ]; then
        blender -b "$blend_dest/$file_name" --python-expr "import bpy; bpy.ops.file.unpack_all()" > /dev/null 2>&1
    fi
}

files=$(find "$src" -maxdepth $level -type f -name "*.blend" 2> /dev/null| wc -l)
echo "Files to be included:     $files"

echo "Start to process..."
find "$src" -maxdepth $level -type f -name "*.blend" 2> /dev/null | sort | while read -r BLEND_FILES
do
    file_name=${BLEND_FILES##*/}
    blend_dest=$(readlink -m "$dest/${file_name%.*}/")
    mkdir -p "$blend_dest"
    blender -b "$BLEND_FILES" --python-expr "import bpy; bpy.ops.file.pack_all()" > /dev/null 2>&1
    mv "$BLEND_FILES" "$blend_dest"
    Organize_files
done
echo "Finished, files exist in: $dest" 
exit 0


