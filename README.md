
# Blender Collector

### Script to pack all textures and files to one blender file
#### `blender-collector` script search after blender files in the directory and organize each file to project directory, blender-collector can also to pack files and move all the resources of the file to own project directory 
#### Usage:
```bash
$ ./blender-collector -s src -d dest 
```
##### The script starts to search blender files from the value in the `-s` parameter <br> and move all the file to the destination by `-d` parameter each file in one directory.<br>by default `blender-collector` search blend files in 1 level depth, you can use `-l` to change the behavior. <br> You can use also with the `-o` flag to collect all the resources of the file like textures to blend file folder 
##### After using  `-o` flag the folder will looks like this <br> 
    projects $ tree
    .
    └── House(Texturing)
        ├── House(Texturing).blend
        └── textures
            ├── 1725 Sun Clouds.jpg
            ├── studio024.hdr
            ├── style_cottage_Flanagan_IMG1.jpg
            ├── wood06.jpg
            ├── WoodFine0024_M.jpg
            └── WoodFine0042_5_L.jpg

###### Author: meirg <br> MIT - 2020
